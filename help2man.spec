%bcond_without nls

Name:           help2man
Summary:        Create simple man pages from --help output
Version:        1.49.3
Release:        1
License:        GPL-3.0-or-later
URL:            http://www.gnu.org/software/help2man
Source:         https://ftp.gnu.org/gnu/help2man/help2man-%{version}.tar.xz

%{!?with_nls:BuildArch: noarch}
BuildRequires:  gcc perl-generators perl(Getopt::Long) perl(POSIX) perl(Text::ParseWords) perl(Text::Tabs) perl(strict) make
%{?with_nls:BuildRequires: perl(Locale::gettext) gettext}
%{?with_nls:BuildRequires: perl(Encode)}
%{?with_nls:BuildRequires: perl(I18N::Langinfo)}

%description
help2man is a tool for automatically generating simple manual pages from program output.

%package_help

%prep
%autosetup -n %{name}-%{version}

%build
%configure --%{!?with_nls:disable}%{?with_nls:enable}-nls --libdir=%{_libdir}/help2man
%{make_build}

%install
make install_l10n DESTDIR=$RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
%find_lang %name --with-man

%files -f %name.lang
%defattr(-,root,root)
%doc README NEWS THANKS
%license COPYING
%{_bindir}/help2man
%{_infodir}/*
%if %{with nls}
  %{_libdir}/help2man
%endif

%files help
%defattr(-,root,root)
%{_mandir}/man1/*

%changelog
* Tue May 16 2023  liyanan <thistleslyn@163.com> - 1.49.3-1
- update version to 1.49.3

* Tue Jan 18 2022 SimpleUpdate Robot <tc@openeuler.org> - 1.48.5-1
- Upgrade to version 1.48.5

* Mon Jul 19 2021 lingsheng <lingsheng@huawei.com> - 1.47.11-1
- Enable support for generating localisted pages

* Thu Nov 07 2019 openEuler Buildtam <buildteam@openeuler.org> - 1.47.11-0
- Package Init
